Sources: 
- https://github.com/waynezhang/foto
- https://fontawesome.com

When adding a new icon, add `width="24" height="24"` to the <svg xmlns> in the SVG file. 

# Install foto
```
brew tap waynezhang/tap
brew install foto
```

# Generate assets
```
make build
```

git commit/push